# Studency

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.1.

## Install angular

1 - Install npm: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm <br />
2 - Install angular running this in your terminal: `npm install -g @angular/cli`

## After every pull

You have to run `npm install` to install every new dependencies installed in the project

## Run your project

1 - `npm run start` <br />
2 - open your browser on http://localhost:4200/

## Angular reload system

the command `npm run start` will execute the command `ng serve` and this command rebuild the project everytime you modify and save a file inside the project

## Angular structure

- node_modules -> Modules installed, it shouldn't be modified by hand <br />
- src -> Application files and folders <br />
  - app -> Application files and folders <br />
    - components -> List of components of this app <br />
      - component.html -> HTML/angular code for your component <br />
      - component.scss -> CSS code for your component <br />
      - component.ts -> typescript code for your component / where you would put back code like request or variables <br />
      - module.ts -> handle import for this component <br />
    - services -> List of services <br />
    - guards -> Contains AuthGuard <br />
    - interfaces -> List of interfaces
    - app-routing.module -> Add a route to this file "routes" when you want to create a new route <br />
  - assets -> Containes image and other extra stuff <br />
  - environments -> Contains environment variables like sql credentials, paths etc.. <br />
- package.json -> Information files abour version, dependencies, app-name... <br /><br />

The file/folder I didn't point out above are less important

## Lexique

- interface: data structure file, like a model. <br />
- service: A service contains method that would be use in multiple component. It can be requests or authentication methods for example. <br />
- guard: implement guard class and can be used to verify if user is connected for example <br />
