export interface Login {
  userid: string;
  password: string;
}

export interface LoginStudent {
  numero: number;
  password: string;
}
