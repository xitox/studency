export interface Domaine {
  id: number;
  nom: string;
  seuil: number;
  description: string;
}
