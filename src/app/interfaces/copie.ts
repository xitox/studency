import { Competence } from './competence';

export interface Copie {
  id: number;
  note: number;
  competence: Competence;
  convocation: {
    numero: number;
  };
  lot: {
    lotNumero: number;
  };
}
