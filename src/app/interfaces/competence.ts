import { Domaine } from './domaine';

export interface Competence {
  id: number;
  nom: string;
  domaine: Domaine;
}
