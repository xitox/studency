import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddWorkComponent } from './components/add-work/add-work.component';
import { HomeComponent } from './components/home/home.component';
import { SeeNoteComponent } from './components/see-note/see-note.component';
import { TeacherDashboardComponent } from './components/teacher-dashboard/teacher-dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { StudentChoiceComponent } from './components/student-choice/student-choice.component';
import { BackOfficeComponent } from './components/back-office/back-office.component';
import { BackOfficeDashboardComponent } from './components/back-office-dashboard/back-office-dashboard.component';
import { ConvocationComponent } from './components/convocation/convocation.component';

const routes: Routes = [
  {
    path: 'secretariat/dashboard',
    component: BackOfficeDashboardComponent,
  },
  {
    path: 'secretariat',
    component: BackOfficeComponent,
  },
  {
    path: 'convocation',
    component: ConvocationComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'choice',
    component: StudentChoiceComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'dashboard',
    component: TeacherDashboardComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'deposer',
    component: AddWorkComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'consulter',
    component: SeeNoteComponent,
    canActivate: [AuthGuard],
  },
  {
    path: '',
    component: HomeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
