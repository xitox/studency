import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'studency';
  id!: string | null;
  constructor(private router: Router, private authService: AuthService) {
    router.events.subscribe(() => {
      this.id = localStorage.getItem('token');
  });
  }
  ngOnInit() {
    this.id = localStorage.getItem('token');
  }

  logout() {
    console.log('logout');
    this.authService.logout();
    this.router.navigate(['/']);
  }
}
