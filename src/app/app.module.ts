import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeModule } from './components/home/home.module';
import { AddWorkModule } from './components/add-work/add-work.module';
import { SeeNoteModule } from './components/see-note/see-note.module';
import { AuthGuard } from './guards/auth.guard';
import { TeacherDashboardModule } from './components/teacher-dashboard/teacher-dashboard.module';
import { StudentChoiceModule } from './components/student-choice/student-choice.module';
import { MatIconModule } from '@angular/material/icon';
import { BackOfficeModule } from './components/back-office/back-office.module';
import { BackOfficeDashboardModule } from './components/back-office-dashboard/back-office-dashboard.module';
import { ConvocationModule } from './components/convocation/convocation.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HomeModule,
    AddWorkModule,
    SeeNoteModule,
    TeacherDashboardModule,
    StudentChoiceModule,
    MatIconModule,
    BackOfficeModule,
    BackOfficeDashboardModule,
    ConvocationModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
