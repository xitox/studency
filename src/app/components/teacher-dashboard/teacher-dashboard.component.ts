import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CopieService } from 'src/app/services/copie.service';
import { AuthService } from 'src/app/services/auth.service';
import { Copie } from 'src/app/interfaces/copie';

@Component({
  selector: 'app-teacher-dashboard',
  templateUrl: './teacher-dashboard.component.html',
  styleUrls: ['./teacher-dashboard.component.scss'],
})
export class TeacherDashboardComponent implements OnInit {
  id!: string | null;
  copieNumber!: FormControl;
  copie!: Copie | null;
  note!: FormControl;

  constructor(
    private router: Router,
    private authService: AuthService,
    private service: CopieService
  ) {}

  ngOnInit() {
    this.id = localStorage.getItem('token');
    this.copieNumber = new FormControl();
    this.note = new FormControl();
    this.copie = null;
  }

  selectCopie(): void {
    this.service
      .loginTeacher(this.copieNumber.value)
      .subscribe((data: Copie) => {
        if (!!data) {
          this.copie = data;
        }
      });
  }
}
