import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-back-office-dashboard',
  templateUrl: './back-office-dashboard.component.html',
  styleUrls: ['./back-office-dashboard.component.scss']
})
export class BackOfficeDashboardComponent implements OnInit {


  importEtudiantsForm = new FormGroup({
    file: new FormControl('', [Validators.required]),
    fileSource: new FormControl('', [Validators.required])
  });

  constructor(private http:HttpClient) { }

  ngOnInit(): void {
  }

  get f() {
    return this.importEtudiantsForm.controls;
  }

  onFileChange(event : any) {
    if(event.target.files.length > 0) {
      const csv = event.target.files[0];
      this.importEtudiantsForm.patchValue({fileSource: csv});
    }
  }

  submit() {
    const formData = new FormData();
    formData.append('csv', this.importEtudiantsForm.get('fileSource')!.value);

    this.http.post('http://localhost:3000/import-csv/upload', formData)
      .subscribe((data => {
       alert('Data importé !')
    }))

  }

}
