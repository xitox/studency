import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SeeNoteComponent } from './see-note.component';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [SeeNoteComponent],
  imports: [
    CommonModule,
    MatCardModule,
    FlexLayoutModule
  ]
})
export class SeeNoteModule { }
