import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentChoiceComponent } from './student-choice.component';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [StudentChoiceComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    FlexLayoutModule
  ]
})
export class StudentChoiceModule { }
