import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './student-choice.component.html',
  styleUrls: ['./student-choice.component.scss'],
})
export class StudentChoiceComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {}

  deposerClick(): void {
    this.router.navigateByUrl('/deposer');
  }
  notesClick(): void {
    this.router.navigateByUrl('/consulter');
  }
  sallesClick(): void {
    this.router.navigateByUrl('/convocation');
  }
}
