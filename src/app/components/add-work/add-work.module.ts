import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddWorkComponent } from './add-work.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';



@NgModule({
  declarations: [AddWorkComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    FlexLayoutModule
  ]
})
export class AddWorkModule { }
