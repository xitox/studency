import { Component, ElementRef, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-back-office',
  templateUrl: './back-office.component.html',
  styleUrls: ['./back-office.component.scss']
})
export class BackOfficeComponent implements OnInit {
  username!: FormControl;
  password!: FormControl;
  message: string = '';

  constructor(private router : Router) { }

  ngOnInit(): void {
    this.username = new FormControl();
    this.password = new FormControl();
  }

  login(): void{
    if (this.username.value === "admin" && this.password.value === "studency") {

      this.router.navigate(['/secretariat/dashboard']);
    }
    else {
      this.message = "Identifiant ou mot de passe  incorrect !"
    }
  }

}
