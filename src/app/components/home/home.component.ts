import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Login, LoginStudent } from 'src/app/interfaces/login';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  userid!: FormControl;
  password!: FormControl;
  studentid!: FormControl;
  studentPassword!: FormControl;

  message: string = '';
  messageStudent: string = '';

  constructor(
    private router: Router,
    private authService: AuthService,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.userid = new FormControl();
    this.password = new FormControl();
    this.studentid = new FormControl();
    this.studentPassword = new FormControl();
    this.authService.logout();
  }

  login(): void {
    const request: Login = {
      userid: this.userid.value,
      password: this.password.value,
    };
    if (!this.userid.value || !this.password.value) {
      this.message = 'Please enter your userid and password';
      return;
    } else {
      this.message = this.authService.loginTeacher(request, '/dashboard');
    }
  }

  loginStudent(): void {
    const request: LoginStudent = {
      numero: this.studentid.value,
      password: this.studentPassword.value,
    };
    if (!this.studentid.value || !this.studentPassword.value) {
      this.messageStudent = 'Please enter your userid and password';
      return;
    } else {
      this.messageStudent = this.authService.loginStudent(request, '/choice');
    }
  }

  accessBackOffice(): void {
    this.router.navigate(['/secretariat']);
  }
}
