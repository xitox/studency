import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConvocationComponent } from './convocation.component';
import { MatCardModule } from '@angular/material/card';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [ConvocationComponent],
  imports: [CommonModule, MatCardModule, FlexLayoutModule],
})
export class ConvocationModule {}
