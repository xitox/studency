import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Copie } from '../interfaces/copie';

@Injectable({
  providedIn: 'root',
})
export class CopieService {
  constructor(private http: HttpClient) {}

  loginTeacher(id: number): Observable<Copie> {
    return this.http.get<any>('http://localhost:3000/copie/' + id);
  }
}
