import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Login, LoginStudent } from '../interfaces/login';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private router: Router, private http: HttpClient) {}

  logout(): void {
    localStorage.setItem('isLoggedIn', 'false');
    localStorage.removeItem('token');
  }

  loginTeacher(request: Login, returnUrl: string): string {
    let message = '';
    this.http
      .post<any>('http://localhost:3000/teacher/login', request)
      .subscribe((data) => {
        if (!!data) {
          localStorage.setItem('isLoggedIn', 'true');
          localStorage.setItem('token', data.userid);
          this.router.navigate([returnUrl]);
        }
        return message;
      });
    message = 'Please check your username and password';
    return message;
  }

  loginStudent(request: LoginStudent, returnUrl: string): string {
    let message = '';
    this.http
      .post<any>('http://localhost:3000/etudiant/login', request)
      .subscribe((data) => {
        if (!!data) {
          localStorage.setItem('isLoggedIn', 'true');
          localStorage.setItem('token', data.numero);
          this.router.navigate([returnUrl]);
        }
        return message;
      });
    message = 'Please check your number and password';
    return message;
  }
}
